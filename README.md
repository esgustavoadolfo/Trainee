# Trainee Developer Test

Este es un test pensado para demostrar las habilidades de los candidatos para integrar el __Área de Ingeniería__ de  __Featuring__.  Esta prueba esta pensada para desarrolladores trainee.

## Problema 1. `Teórico`

Una empresa necesita guardar registro de sus ventas. El sistema tiene que soportar la existencia de productos, categorias de productos, asi como las ventas de estos.
Cada producto tendra un nombre, un valor, una categoria de productos, asi como la cantidad disponible de estos a modo de inventario. Las ventas pueden soportar la capacidad de ser asociadas a un cliente.

### Modelo de datos

Escriba a continuación las tablas que utilizaría para resolver este problema con los campos y llaves de éstas. Intente hacer el modelo lo más robusto posible, pero sin incluir datos adicionales a los que se plantean acá.

### SQL
Considerando el enunciado anterior conteste las siguientes preguntas:

1. Escriba una Query que entregue la lista de los productos asociados a la categoria "Vegetales"
2. Escriba una Query que calcule la suma de las ventas.
3. Escriba una Query que entregue el producto mas vendido.
4. Escriba una Query que indique el numero de compras que ha efectuado cada cliente registrado.

## Problema 2. `Práctico Indicadores`

Desarrollar una Aplicación Web que permita visualizar el valor actual (En tiempo real) de distintos tipos de cambio para monedas. 

Se pide que como minimo pueda visualizarse el valor del dolar y la uf, pero se deja a imaginación del desarrollador la inclusión de otros indicadores y formas de visualización.

Se puede obtener la información del precio actual de los tipos de cambio con esta API: https://mindicador.cl/api, pero puede hacer uso de API's complementarias para obtener otros tipos de información.

1. *Diseño de la Interfaz de Usuario:* Diseña la interfaz de usuario de tu aplicación web. Debe incluir al menos una sección para mostrar el valor actual del dólar y la UF. Puedes considerar la inclusión de otros tipos de cambio y formas de visualización, como gráficos, tablas, etc.

2. *Obtención de Datos:* Utiliza la API de Mindicador (https://mindicador.cl/api) para obtener los valores actuales de las monedas que deseas mostrar, como el dólar y la UF. Puedes hacer solicitudes HTTP a esta API para obtener los datos en tiempo real.

3. *Procesamiento de Datos:* Una vez que obtengas los datos de la API, procesa la respuesta para extraer la información relevante, como el valor del dólar y la UF.

4. *Visualización de Datos:* Muestra los valores obtenidos en la interfaz de usuario. Puedes usar HTML, CSS y JavaScript para crear elementos que muestren los valores de cambio.

5. *Actualización en Tiempo Real:* Para lograr la actualización en tiempo real, puedes usar la función setInterval de JavaScript para realizar solicitudes periódicas a la API y actualizar los valores en la interfaz de usuario sin necesidad de recargar la página.

6. *Opciones Adicionales:* Si deseas agregar más funcionalidades, considera permitir a los usuarios seleccionar las monedas que desean visualizar, agregar un historial de valores, y aplicar estilos y gráficos para mejorar la presentación de los datos.

## Problema 3. `Práctico Pokemon`
Usando la API de Pokémon (https://pokeapi.co/), debes realizar las siguientes tareas:

1. *Diseño de la Interfaz de Usuario:* Diseña la interfaz de usuario de tu aplicación web. Debe incluir elementos para buscar y mostrar información sobre los Pokémon, como nombres, imágenes, tipos, habilidades, etc.
2. *Búsqueda de Pokémon:* Permite a los usuarios buscar Pokémon por nombre o número de Pokédex y realiza solicitudes a la API de Pokémon para obtener información detallada sobre el Pokémon seleccionado.
3. *Visualización de Datos:* Muestra la información del Pokémon en la interfaz de usuario. Puedes usar imágenes, tablas o tarjetas para mostrar los datos de manera atractiva.
4. *Listado de Pokémon:* Considera incluir una lista de Pokémon populares o la capacidad de navegar a través de diferentes Pokémon para acceder a su información.
5. *Información Adicional:* Puedes agregar características adicionales, como la posibilidad de ver la evolución de un Pokémon, detalles sobre sus tipos y habilidades, y otros datos relevantes.
6. *Estilos y Temas:* Personaliza la apariencia de tu aplicación web con CSS para que se vea atractiva y relacionada con el tema de Pokémon.

## Problema 4. `Práctico Rick and Morty`
Usando la API de Rick and Morty (https://rickandmortyapi.com/), debes realizar las siguientes tareas:

1. *Diseño de la Interfaz de Usuario:* Diseña la interfaz de usuario de tu aplicación web. Debe incluir elementos para buscar y mostrar información sobre personajes, episodios y ubicaciones de la serie.
2. *Búsqueda de Información:* Permite a los usuarios buscar personajes, episodios o ubicaciones por nombre, número de episodio o cualquier otro criterio relevante. Realiza solicitudes a la API de Rick and Morty para obtener información detallada.
3. *Visualización de Datos:* Muestra la información en la interfaz de usuario de manera atractiva. Puedes utilizar tarjetas, tablas, imágenes y otros elementos para presentar los datos.
4. *Detalles y Conexiones:* Para personajes, muestra detalles como nombre, género, especie y episodios en los que aparece. Para episodios, muestra detalles como nombre, número de episodio y personajes que participan. Para ubicaciones, muestra detalles como nombre y dimensiones.
5. *Exploración y Navegación:* Permite a los usuarios explorar personajes, episodios y ubicaciones relacionados. Puedes incluir enlaces y botones que permitan navegar a través de la información relacionada.
6. *Estilos y Temas:* Personaliza la apariencia de tu aplicación web con CSS para que se relacione con el tema de Rick and Morty y sea atractiva.
7. *Funcionalidades Adicionales:* Puedes considerar agregar funcionalidades como guardar personajes favoritos, mostrar episodios relacionados o filtrar ubicaciones por dimensiones, entre otras.

## Variante para aplicacion backend
Debes desarrollar una aplicación de backend que interactúe con las API de indicadores, Pokémon o Rick and Morty, puedes seguir estos pasos generales:

1. *Configuración del Entorno de Desarrollo:* Configura un entorno de desarrollo adecuado para tu lenguaje de programación preferido. Puedes utilizar lenguajes como Node.js, Python, Ruby, Java, o cualquier otro que te resulte cómodo.

2. *Gestión de Rutas y Endpoints:* Define las rutas y endpoints en tu aplicación de backend para manejar las solicitudes de los clientes. Por ejemplo, puedes definir rutas para obtener indicadores, Pokémon o datos de Rick and Morty.

3. *Conexión con las API Externas:* Utiliza bibliotecas o módulos de tu lenguaje de programación para realizar solicitudes HTTP a las API externas correspondientes. Por ejemplo, puedes usar Axios, Requests o bibliotecas similares para realizar solicitudes a las API de Mindicador, PokeAPI y Rick and Morty.

4. *Procesamiento de Datos:* Una vez que obtengas los datos de las API externas, procesa la respuesta para extraer la información relevante y formatearla según tus necesidades.

5. *Almacenamiento en una Base de Datos (Opcional):* Si deseas mantener un registro de los datos o realizar operaciones más complejas, considera almacenar los datos en una base de datos local, como MySQL, PostgreSQL o MongoDB.

6. *Creación de Endpoints Personalizados:* Crea endpoints personalizados para que los clientes de tu API puedan acceder a los datos procesados. Por ejemplo, podrías crear un endpoint que devuelva el valor actual del dólar y la UF para tu aplicación de indicadores.

7. *Documentación de la API:* Documenta los endpoints y cómo los usuarios pueden interactuar con tu API. Puedes utilizar herramientas como Swagger o Postman para crear documentación de API.

8. *Seguridad y Autenticación (Opcional):* Si es necesario, implementa medidas de seguridad, autenticación y autorización para proteger tu API de accesos no autorizados.

9. *Pruebas y Depuración:* Realiza pruebas exhaustivas para garantizar que tu aplicación de backend funcione correctamente y maneje errores de manera adecuada.

10. *Implementación y Despliegue:* Despliega tu aplicación de backend en un servidor web o plataforma de hosting. Asegúrate de que esté disponible para los clientes que deseen interactuar con ella.

11. *Monitoreo y Mantenimiento:* Implementa herramientas de monitoreo para seguir el rendimiento de tu aplicación de backend y realiza mantenimiento periódico para corregir errores y agregar nuevas características.

### Instrucciones

Crear un branch a partir de la rama `master` con tu nombre y apellido, por ejemplo `juanito-perez`.

Para el desarrollo web de la solución __se deben__ ocupar como minimo, las siguientes tecnologias: **ECMAScript** o **HTML5** y **CSS** y **Angular** o **React** o **VueJS**

Para el desarrollo backend de la solución __se deben__ ocupar como minimo, las siguientes tecnologias: **NodeJS**, **postgresSQL**, **MongoDB**, **Firebase**

Se debe sobreescribir este README.md con las instrucciones para poder probar la aplicación.

Se debe describir brevemente en un archivo md cual fue el planteamiento de la solución y la motivación para postular al cargo.

**Nota:** El uso de buenas practicas de codificación siempre es bienvenido.

***Exito***
